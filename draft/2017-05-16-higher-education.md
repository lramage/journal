---
layout: post
title: "Higher Education"
date: 2017-05-16 3:33:18
categories: learning college university
---

“Self-education is, I firmly believe, the only kind of education there is.” - Isaac Asimov

- [Hacking Higher Education](https://joshkaufman.net/hacking-higher-education-clep/)
- [MIT Challenge](https://www.scotthyoung.com/blog/myprojects/mit-challenge-2/)
- [Saylor Academy](https://www.saylor.org/)
- [Thomas Edison University](http://www.tesu.edu)
- [MIT OpenCourseWare](https://ocw.mit.edu/)
- [Excelsior College](http://www.excelsior.edu)
