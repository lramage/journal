---
layout: post
title: Operating System Unit Testing
date: 2017-08-04 01:39:00
categories: linux operating systems imaging unit testing
---

- [buildbot](https://buildbot.net/)

- [Phoronix Test Suite](https://www.phoronix-test-suite.com/)

- [Bonnie++](http://www.coker.com.au/bonnie++/)
- [hdparm](https://sourceforge.net/projects/hdparm/)

- [Ansible](https://www.ansible.com/)
- [Salt](https://saltstack.com/)
