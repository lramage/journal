---
date: 2020-05-30
categories: gentoo linux bluetooth
---

# Bluetooth Hacking on Gentoo Linux

```sh
echo "net-wireless/bluez-tools ~amd64" > /etc/portage/package.accept_keywords

emerge -nq net-wireless/bluez \
           net-wireless/bluez-hcidump \
           net-wireless/bluez-tools
```

## Reference

- [ath3k](https://wireless.wiki.kernel.org/en/users/Drivers/ath3k)

- [Bluetooth - Gentoo Wiki](https://wiki.gentoo.org/wiki/Bluetooth)
