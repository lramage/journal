---
layout: post
title: Docker vs LXC
date: 2018-04-27 10:46:56
categories: containers lxc docker linux
---

References:

- [baseimage-docker](https://github.com/phusion/baseimage-docker)

- [.dotfiles/.local/etc/docker/daemon.json](https://github.com/lramage94/dotfiles/blob/master/.local/etc/docker/daemon.json)

- [Docker vs. LXC: There Can Only Be One](https://kalmanolah.net/docker-vs-lxc-there-can-only-be-one.html)

- [JeOS](https://en.wikipedia.org/wiki/Just_enough_operating_system)

- [nsenter](http://man7.org/linux/man-pages/man1/nsenter.1.html)

- [runit](http://smarden.org/runit/)
