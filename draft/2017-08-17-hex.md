---
layout: post
title: Hex OS
date: 2017-08-17 01:50:00
categories: linux hypervisor gentoo virtualization containers qemu
last_modified_at: 2018-06-03 01:06:22
---

## Core

Examples:

Gentoo

`profiles/base/packages`

`sys-apps/baselayout/baselayout-9999.ebuild`

`proj/baselayout`

Linux From Scratch (LFS)

Dependencies: http://www.linuxfromscratch.org/lfs/view/stable/wget-list

Acronyms:

- Hypervisor EXtended

- Hex {Emulates,Embraces,Embodies} uniX


See also:

- EFI SHELL

References:

- [CoreOS](https://coreos.com/)

- [Dell DKMS](https://github.com/dell/dkms)

- [FreeBSD Source Tree](https://svnweb.freebsd.org/base/release/11.1.0)

- [Manjaro Hardware Detection](https://github.com/manjaro/mhwd)

- [VMware ESXi](https://en.wikipedia.org/wiki/VMware_ESXi)

- [Xen](https://www.xenproject.org/)
