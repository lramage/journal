---
layout: post
---

(Dependencies)
<pre>
$ emerge xdotool
</pre>

Wake up monitor from terminal (via ssh)
<pre>
$ xset dpms force on
</pre>

# References
- [xset](https://www.x.org/archive/X11R7.5/doc/man/man1/xset.1.html "xset")
- [What is DPMS?](https://en.wikipedia.org/wiki/VESA_Display_Power_Management_Signaling "dpms")
- [DPMS Reference](https://wiki.archlinux.org/index.php/Display_Power_Management_Signaling "dpms")
