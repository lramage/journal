---
date: 2020-05-30
categories: alpine linux rpi2
---

# Alpine Linux on the Raspberry Pi 2 Model B

Source: [`usercfg.txt`](https://gitlab.com/lramage/dotfiles/-/blob/master/.inventory/rpi2b/usercfg.txt)

## Reference

- [config.txt - Raspberry Pi Documentation](https://www.raspberrypi.org/documentation/configuration/config-txt)

- [Persistent Installation on RPi3](https://web.archive.org/web/20171125115835/https://forum.alpinelinux.org/comment/1084#comment-1084)

- [Raspberry Pi 2 Model B](https://www.raspberrypi.org/products/raspberry-pi-2-model-b)

- [Raspberry Pi - Alpine Linux Traditional disk-based (sys) installation](https://wiki.alpinelinux.org/wiki/Raspberry_Pi#Traditional_disk-based_.28sys.29_installation)

