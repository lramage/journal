---
layout: post
date: 2017-04-07 10:26:31
categories: writing citations website
---

# Website Citation

## Reference

- [Citing Sources Research Guide](https://libguides.richmond.edu/citingsources)

- [Writer's Web: MLA Documentation: Printed & Other Sources](http://writing2.richmond.edu/writing/wweb/mladocu.html)

