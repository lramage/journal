---
layout: post
title: Portable Continuous Integration With Docker
date: 2018-12-10 02:51:00
categories: continous integration docker linux software forge
---

Proprietary Continuous Integration Workflows for Free Software Projects?

[]: https://dwheeler.com/flawfinder
[]: https://github.com/buildbot/buildbot
[]: https://github.com/coreinfrastructure/best-practices-badge/blob/master/doc/criteria.md
[]: https://github.com/docker/docker-ce
[]: https://github.com/genuinetools/img
[]: https://gitlab.com/gitlab-org/gitlab-ce
[]: https://github.com/jenkinsci
[]: https://www.gnu.org/philosophy/who-does-that-server-really-serve.html
