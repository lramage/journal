---
layout: post
title: PXE Boot Server
date: 2018-04-04 02:32:24
categories: linux pxe boot server dnsmasq syslinux
---

References:

- [.dotfiles/.local/usr/local/portage/dotfiles/lxc-container/pxe-server]()

- [Network Booting with DNSMASQ in Proxy Mode](https://blog.n0dy.radio/2014/09/14/network-booting-with-dnsmasq-in-proxy-mode/)
