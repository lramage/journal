---
layout: post
title: Hostname Schema
date: 2018-04-02 05:03:09
categories: linux hostname dns schema domain administration system
---

References:

- [Simple Naming Scheme](https://github.com/wtcross/simple-naming-scheme)
- [Convention for Semantic Hostnames](https://github.com/semantic-hostnames/semantic-hostnames)
