![](http://i.imgur.com/8e7NOAo.png)

I recently read an article entitled ["50 Places Linux is Running That You Might Not Expect"](http://www.comparebusinessproducts.com/fyi/50-places-linux-running-you-might-not-expect)
, and while it was encouraging to see governments, corporations, and other large organizations
choosing to use free and open source software, there was a part of the article that troubled me. 
The article is from 2010.

- [RHEL Blog Post](https://www.redhat.com/en/about/blog/us-department-defense-releases-security-configuration-standard-red-hat-enterprise-linux-7) from April 24, 2017
