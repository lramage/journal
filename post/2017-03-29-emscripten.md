---
layout: post
title: Emscripten failing
date: 2017-03-29 10:14:07
categories: log web assembly emscripten
---

So this evening I attempted to setup emscripten on my laptop and I had quite a few mishaps,
more than likely due to my own misguidedness. I must admit that I did not work on it for very long
but I do believe that I am missing a step somewhere. I attempted to use the binary package for my
linux distro, then downloaded the latest stable version from the website, and finally the latest 
repository on github, but all three of them were not working for me. Every time I ran *emsdk activate*
it crashed. Perhaps it is my environment? In any case, there will be more of this foolishness to follow.

I have gotten out of sync with my writing and I have been trying to catch up. Is it cheating to
write a journal of my day, the day after it has happened?
