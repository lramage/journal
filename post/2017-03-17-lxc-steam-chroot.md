---
layout: post
title: LXC, Steam, Chroot
date: 2017-03-17 7:47:30
categories: log
---

Tonight I worked on my arch linux chroot install of steam. I am still trying to figure out my audio
on this system. It's pretty frustrating I have to admit. Also I found a [little work](https://www.reddit.com/r/archlinux/comments/19h2oi/libgl_problems/) around for
permissions for the graphics card inside chroot. Regular windows are running but when it comes timeto launch a game half of my games are failing and the ones that succeed are dreadfully slow. I enjoy figuring things like this out but I really just wanna play some games this weekend!

Here are a few articles I read today
- [lxc container without systemd](https://www.claudiokuenzler.com/blog/625/create-lxc-container-debian-jessie-without-systemd#.WMvs6XXythE)
- [Why Lisp?](http://www.gigamonkeys.com/book/introduction-why-lisp.html)

Here are a few resources that came in handy today for my linux gaming issues
- [Steam Overlay is Broken](https://github.com/ValveSoftware/steam-for-linux/issues/3093)
- [Borderlands 2 Windowed Mode](http://askubuntu.com/questions/546981/borderlands-2-will-only-starts-in-a-small-window-ubuntu-14-04-64bit)
