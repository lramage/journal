---
layout: post
title: "The Trinity Foundation: Math and the Bible"
date: 2017-04-02 09:44:52
categories: science mathematics philosphy bible
---

Over the past few days, I have been studying mathematics,
physics, and logic in hopes of becoming a better programmer.
This has lead me down the path of some of the great scientific
minds of our time, particularly Albert Einstein. I have been
learning about his "Theory of Relativity" but I have also begun
to study his early life and personal relationships, and also
[his views on religion and philosphy](https://en.wikipedia.org/wiki/Religious_and_philosophical_views_of_Albert_Einstein).

During these studies, I began to ponder the idea of how mathematics and the bible
are connected. I am a Christian and my faith is the basis for all that I do. I believe in
a personal God who has created me specifically for the purpose of glorifying him through
programming in particular, but also in every aspect of my life.

I found a review for "Math and the Bible" on a website which upon closer inspection,
seems to echo the ideas that I have been incubating in my mind.

["Math and the Bible" - J.C. Keister](http://www.trinityfoundation.org/journal.php?id=55)
