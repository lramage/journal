---
layout: post
title: "Canon Linux Drivers"
date: 2017-06-02 08:00:03
categories: linux printing drivers canon
---

A week or two ago (I can't remember), I bought my wife a [Canon PIXA MG3620 Printer](https://www.usa.canon.com/internet/portal/us/home/support/details/printers/inkjet-multifunction/mg-series-inkjet/pixma-mg3620) so that she can work on her party/event
planning business. In Nigerian Culture, parties and events are huge and she hasn't been very impressed since coming to America.
So we bought this printer, and the instructions that came with it included an Android App for wireless printing. 
The printer did not even come with a USB type B cable. I was a bit concerned with installing this device on my wife's computer.
This is just a generic Ubuntu LTS installation. She does not appreciate Linux or any other OS; She just simply wants a web browser
and a document editor and no hassle. To my surprise, Canon has the drivers on their website in either Deb or RPM and even source format.
It took me about five minutes to download and install, and I was printing wirelessly on Linux just as easily as it was from the Android
app! So thank you, Canon, for your efforts in making printing on Linux just as simple as any other platform.
