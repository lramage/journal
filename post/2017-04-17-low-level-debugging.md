---
layout: post
title: "Low Level Debugging"
date: 2017-04-17 12:39:36
categories: c assembly debugging low level repl
---

Recently, I have been learning how to compile freebsd kernel modules for use on
my macbook pro from work. It's interesting to see how similar and yet completely alien
this system is from Linux. Some of the basics are still there, for instance, by default
freebsd does not come with bash. That was one of the first packages I installed. I also
miss *pciutils*, *usbutils*, and *iproute2*. Fortunately, two out of three of those
packages have ports that can be installed, but sadly, iproute2 is linux only. The
same can be said for iptables. Freebsd has *pf* and *ipfw* for setting up a firewall.

There are so many programming languages out there and so little time to learn them all.
I am focusing on what I call the "sacred trinity": Assembly, C, and Lisp, particularly,
common lisp. My long term goal is to be able to be able to write drivers for the various
UNIX operating systems.

Here are some resources I discovered today.

- [Debugging with GDB](ftp://ftp.gnu.org/old-gnu/Manuals/gdb/html_chapter/gdb_toc.html)
- [Debugging Assembly code with gdb](http://www.akira.ruc.dk/~keld/teaching/CAN_e14/Readings/gdb.pdf)
- [Interactive GCC](http://www.artificialworlds.net/wiki/IGCC/IGCC)
