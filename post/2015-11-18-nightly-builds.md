---
layout: post
title: "Using Nightly Builds"
date: '2015-11-18T08:53:24-05:00'
---

I have an old moto g first gen that I got a few years back. It’s a pretty decent little phone considering the price. First thing that I did when I bought it was flash the stock rom. I used the slimkat rom on it for a couple of years and it worked pretty well with only a few bugs. Ever since lollipop came out I have been dying to upgrade my phone but the slimrom project has been kinda dead for awhile. So I decided to try cyanogen mod 12. I have used CM before and it’s a great rom. I prefered slimrom because of the minimalistic feel it had and the pure black theme that it came with by default. I searched the builds for my phone but all they had were nightly builds. That kinda spooked me a little. I didn’t want to use something that was unstable. So I stuck with slimkat and grudgingly waited to see when I would get a stable build of CM12. Well about a week ago, my wife got a new phone and I was so jealous of the new software that came on it so I decided that I would just try the nightly builds and if they didn’t work then I could always go back to slimrom. To my surprise, I have had no issues with nightly builds and my phone is looking pretty sick.
