---
layout: post
title: "Seeing Red"
date: 2017-04-11 08-03-57
categories: redshift linux
---

As a professional programmer, I spend my entire day sitting in front of at least one, or more digital displays. 
Now while we are encouraged to get up and take frequent breaks, and while we also have a daily walk around the building at 3 o'clock,
I am afraid my poor eyes are still suffering from all the bright blue light. To compound the matter, I then come home and write this
journal, and "play", as I call it, with different applications and programming languages. I simply spend way too much of my time
on computers. Oh but it is marvelous! However, I have been suffering from eye strain as of late, and I am reading up on ergonomics
and best practices to keep this frail, and feeble physical body from premature failure. I spent much of my time last year in the
hospital, and around doctor, and it was not an enjoyable experience. Regrettably, due to my genetics there is only so much that can
currently be done with crohn's disease, however, I am going to do my best with what I have. 

That being said, I have known about [redshift](http://jonls.dk/redshift/) for quite a few years and even tried [f.lux](https://justgetflux.com/),
on which it was based but I did not really understand the benefits of using such an application. Well now I am older and I am definitely appreciating
these programs. I also found it interesting, how it uses latitude and longitude to calculate your location so that it can determine the color spectrum
based on how much sunlight I have. I also learned that Sarasota, FL is located at roughly twenty seven degrees north, by minus eighty-two degrees west.
I rounded to whole numbers because I am lazy.

- [dotfiles/.config/redshift.conf](https://raw.githubusercontent.com/lramage94/dotfiles/master/.config/redshift.conf)

