---
layout: post
title: Preservation
date: 2017-06-21 01:02:47
categories: library science archiving preservation decision making
---

Git, and other version control software, is a great tool for keeping track of software projects.
However, I am often conflicted over what I actually need to keep, and what can be thrown away.
There are many different git branching strategies that utilize many small, disposable branches
in order to foster a clean master branch. There are also many repository structures that prefer
to keep as many changes as possible, in order to get a full history of what has transpired.

Library Science: [Preservation Criteria](https://en.wikipedia.org/wiki/Preservation_(library_and_archival_science)#Decision_making_and_criteria)

1) uniqueness
2) irreplaceability
3) high level of impact – over time or place
4) high level of influence 
5) representation of a type
6) comparative value
