---
layout: post
title: "Alpine Linux - soundcard.h"
date: 2017-04-28 10:53:56
categories: alpine linux festival cmusphinx soundcard
---

Tonight after work I continued my pursuit of building an AI with alpine linux on a raspberry pi. I am running into
quite a bit of trouble with some of my dependencies. Both Festival, and Pocket Sphinx are failing to compile.
Here is the error that I am getting:

<pre>

  $ make
  make .... # misc
  In file included from ad_oss.c:63:0:
  /usr/include/sys/soundcard.h:1:29: fatal error: linux/soundcard.h: No such file or directory
    #include <linux/soundcard.h>
    
  compilation terminated.
  ...
  
</pre>

I cannot install *libasound2-dev* but I know for a fact that I have that header in */usr/src/linux-headers-4.4.52-0-rpi/*
on my alpine linux installation.

- [Pocketsphinx - alsa instead of pulseaudio](https://raspberrypi.stackexchange.com/questions/26517/how-to-make-pocketsphinx-use-alsa-instead-of-pulseaudio-or-jack)
- [CMU Sphinx - Raspberry Pi](http://cmusphinx.sourceforge.net/wiki/raspberrypi)

I am also attempting to setup bluetooth. But since I have a pi 2 I cannot use the instructions on the wiki as-is.

- [Alpine Linux - Raspberry Pi 3 Bluetooth](https://wiki.alpinelinux.org/wiki/Raspberry_Pi_3_-_Setting_Up_Bluetooth)
