---
layout: post
title: "Digitech effect pedal under Linux"
date: 2017-05-04 09:36:22
categories: linux music digitech guitar pedal audio
---

A friend gave me a Digitech RP500 almost a year ago and it has been sitting in my closet until today.
I finally bought a second instrument cable so that I can hook the pedal up to my amp.
It's going to take a bit of time getting used to using the pedal. I spent the evening adjusting
my amp and pedal settings and exploring all of the different effects and tones.

Digitech came out with a software update for this pedal and so I am going to try to install
the net updater tool in wine. The new software includes a 20 second looper which, besides the 
envelope filter and wah effect, is something that I have been wanting to purchase for a very long time.

I also discovered, that a [good fellow](http://desowin.org/) wrote a editing tool for this pedal for linux!

- [Digitech RP500 - Downloads](http://digitech.com/en-US/products/rp500#downloads)
- [Digitech RP500 - Owner's Manual](https://3e7777c294b9bcaa5486-bc95634e606bab3d0a267a5a7901c44d.ssl.cf2.rackcdn.com/product_documents/documents/234_1466010175/RP500_Manual_5059478-A_original.pdf)
- [gdigi](http://desowin.org/gdigi/)
