---
layout: post
title: Why I prefer UNIX
date: 2018-06-25 04:01:29
categories: unix programming philosophy functional
---

"...modularity means more than modules. Our ability to decompose
a problem into parts depends directly on our ability to glue solutions
together. To support modular programming, a language must provide good
glue...Smaller and more general modules can be reused more widely, easing subsequent programming" [(Hughes, 22)][1].

"Write programs that do one thing and do it well" [(Raymond)][2].

"C is not a "very high level" language, nor a "big" one, and is not specialized to any particular area of application.
But its absence of restrictions and its generality make it more convenient and effective for many tasks than supposedly more powerful languages.
C was originally designed for and implemented on the UNIX operating system on the DEC PDP-11, by Dennis Ritchie" [(Kernighan and Ritchie, ix)][3].

"UNIX is not popular because it is the best operating
system one could imagine, but because it is an extremely flexible system which is easy to
extend and modify. It is an ideal platform for developing new ideas" [(Burgess, 5)][4].

<!-- References: -->

[1]: https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf
[2]: http://www.catb.org/~esr/writings/taoup/html
[3]: https://archive.org/details/TheCProgrammingLanguageFirstEdition
[4]: http://pdplab.it.uom.gr/project/sysadm/unix.pdf