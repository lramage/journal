---
layout: post
title: Setting Up Gerrit With MYSQL
date: '2016-12-19T09:53:14-05:00'
---
As of Gerrit 2.13.4, when setting up a new instance with mysql, be sure to set explicit_defaults_for_timestamp = true in your mysqld.cnf located at /etc/mysql/mysql.conf.d/ in Debian based distributions.
Then to initialize gerrit we will need to add a service to systemd. Copy the script below to /etc/systemd/system and then to enable it we will use

```
    $ sudo systemctl enable gerrit.service
    $ sudo systemctl start gerrit.service
```

