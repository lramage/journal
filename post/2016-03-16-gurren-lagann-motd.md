---
layout: post
title: Gurren Lagann motivation quotes for linux message of the day
date: '2016-03-16T22:04:03-04:00'
---

Listen up, Simon. Don’t believe in yourself. Believe in me! Believe in the Kamina who believes in you!

- [Debian: MOTD](https://wiki.debian.org/motd)
