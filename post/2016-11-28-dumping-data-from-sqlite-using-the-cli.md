---
layout: post
date: '2016-11-28T07:36:17-05:00'
---

# Dumping SQLite databases from the Terminal

First, open the database:

```sh
sqlite3 .db
```

Then set the mode to `CSV` and turn on headers:

```sh
.mode csv
.headers on
```

Then set the output file:

```sh
.out .dmp
```

Finally, query the database for it to be exported to the file.

```SQL
SELECT * FROM {{table_name}};
```

After exiting `sqlite`, we can then see the output file in the current working
directory. We can then `cat` out the file to see the data from our table.
