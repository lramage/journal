---
layout: post
title: Phoronix Test Suite on Android
date: 2018-05-18 02:30:40
categories: linux android benchmarking testing phoronix gpl termux
last_modified_at: 2018-05-24 04:07:01
---

I am investigating two methods,

a) Termux

b) adb (via usb for now, but also over the network)

I will need to work on option the second one, but my changed for Termux have already been merged upstream.

[github/phoronix-test-suite/commit/8651564](https://github.com/phoronix-test-suite/phoronix-test-suite/commit/865156461a653302dbdc8af8f1a87a922b3a5a2a)

Termux Dependencies

    pkg install git php
    
Installation

    git clone https://github.com/phoronix-test-suite/phoronix-test-suite

    cd phoronix-test-suite && ./install-sh /data/data/com.termux/files/usr
       
Phoromatic over WiFi

    ip a | grep "inet " # to get your phone's ip address

    phoronix-test-suite start-phoromatic-server
    
    # watch it when it starts up to see which random port is assigned to the web server
    # or set a default port in the user configuration file.
    
Current Issues

```
Warning: session_start(): open(/tmp/sess_33188dd94e8186ca17799c1182142aa5, O_RDWR) failed: No such file or directory (2) in /data/data/com.termux/files/usr/share/phoronix-test-suite/pts-core/phoromatic/phoromatic_functions.php on line 119

Warning: session_start(): Failed to read session data: files (path: /tmp) in /data/data/com.termux/files/usr/share/phoronix-test-suite/pts-core/phoromatic/phoromatic_functions.php on line 119

Warning: Cannot modify header information - headers already sent by (output started at /data/data/com.termux/files/usr/share/phoronix-test-suite/pts-core/phoromatic/phoromatic_functions.php:119) in /data/data/com.termux/files/usr/share/phoronix-test-suite/pts-core/objects/pts_webui.php on line 114
```
    
Eventually I will create a page on the [Termux Wiki](https://wiki.termux.com/wiki/Phoronix-test-suite), but currently, I do not have permission to do so.

Phoronix-test-suite can also be run on postmarketOS by enabling the `edge` repository.

[aports/testing/phoronix-test-suite](https://github.com/alpinelinux/aports/blob/master/testing/phoronix-test-suite/APKBUILD)*

*this package just so happens to be maintained by yours truly.

References:

- [Alpine Linux Package Contents - Phoronix Test Suite](https://pkgs.alpinelinux.org/contents?file=&path=&name=phoronix-test-suite&branch=edge&repo=testing&arch=armhf)

- [Phoronix Test Suite - Installer Script](https://github.com/phoronix-test-suite/phoronix-test-suite/blob/master/install-sh)

- [Phoronix Test Suite - Feature: Android Support](https://github.com/phoronix-test-suite/phoronix-test-suite/issues/253)
