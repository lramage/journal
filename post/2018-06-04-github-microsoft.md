---
layout: post
title: Migrating to Gitlab
date: 2018-06-04 09:49:35
categories: github microsoft gitlab
last_modified_at: 2018-06-08 10:17:11
---

I deleted Github.

> Microsoft has been a developer-focused company from the very first product we created
> to the platforms and tools we offer today. Building technology so that others can build 
> technology is core to our mission to empower every person and every organization on the planet to achieve more.

However, Bill Gates accused hobbyists and free software developers of stealing said product.

> And Microsoft is all-in on open source. We have been on a journey with open source, 
> and today we are active in the open source ecosystem, we contribute to open source projects, 
> and some of our most vibrant developer tools and frameworks are open source.
> When it comes to our commitment to open source, judge us by the actions we have taken in the recent past,
> our actions today, and in the future.

Three words: Embrace, Extend, Extinguish.

> We both believe GitHub needs to remain an open platform for all developers. 
> No matter your language, stack, platform, cloud, or license, GitHub will continue 
> to be your home—the best place for software creation, collaboration, and discovery.

I hope so, but I honestly don't trust it.

I've been wanting to for awhile and this Microsoft thing finally tipped me over the edge. 

Yes, I know that gitlab.com (running gitlab enterprise edition) is not Free Software, 
however,the community edition is and I've been using it for awhile now anyway so I just had to migrate the rest of my repos over.

References:

- [A bright future for GitHub](https://blog.github.com/2018-06-04-github-microsoft)

- [Halloween Documents](https://en.wikipedia.org/wiki/Halloween_documents)

- [Microsoft + GitHub = Empowering Developers](https://blogs.microsoft.com/blog/2018/06/04/microsoft-github-empowering-developers)

- [Open Letter to Hobbyists](https://en.wikipedia.org/wiki/Open_Letter_to_Hobbyists)
