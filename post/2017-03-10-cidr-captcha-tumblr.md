---
layout: post
title:  CIDR, Captcha, Tumblr
date:   2017-03-10 7:06:12
categories: log 
---

I have been mucking around in kernel routing tables all day at work. Every day in fact, for the last week or so.
But it is so good because I have been learning so much. I learned about [CIDR]([200~https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing "Wikipedia")
and static routing tables and bit masking. I knew these things on an academic level. I remember studying for my
Comptia A+ exam in high school and learning about subnet masks and such. But now I am actually building and
debugging software that uses it. It's very challenging but also very rewarding.

I read [an article](https://www.troyhunt.com/breaking-captcha-with-automated-humans/ "breaking captcha")
today about different "services" out there that offer an api for cracking reCaptchas for hackers.
It makes me angry to see people using technology for the wrong things but it also makes me marvel
at how they could even come up with these ideas.

For some strange reason, my posts that I imported from Tumblr aren't displaying correctly? They are just
simple markdown just like my regular posts. It's a bit frustrating but I will figure it out.
I had time to think about my new static site generator idea today. The pieces are starting to fall into place.
I would like to keep exploring Jekyll and all that it has to offer so that I can reuse the concepts that I like.

Tonight I am giving my [Gateway 450 ROG](https://www.cnet.com/products/gateway-450rog-series/specs/ "CNET")
 a little bit of love. I had installed FreeBSD on it and was using it for asm programming in my free moments
(which are few and far between) but I had not installed the wireless driver for it yet.
