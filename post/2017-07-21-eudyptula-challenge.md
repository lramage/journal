---
layout: post
title: Eudyptula Challenge
date: 2017-07-21 09:40:14
categories: c linux kernel challenge programming software
---


This sounds like an amazing challenge. Sadly, they are not accepting anymore applicants.

- [Eudyptula Challenge](http://eudyptula-challenge.org/)
- [Linux Foundation Events - Eudyptula Challenge](https://events.linuxfoundation.org/sites/events/files/slides/ec.pdf)
