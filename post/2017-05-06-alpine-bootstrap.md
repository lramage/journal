---
layout: post
title: "Bootstrap Alpine from Ubuntu (Magic Stick)"
date: 2017-05-06 08:33:27
categories: alpine linux bootstrap ubuntu magic stick asus t100ta
---

Today I spent quite a few hours trying to bootstrap alpine linux on my asus t100ta from ubuntu on the magic stick.

Here is my partitioning layout:

<pre>
/dev/mmcblk0p1  /mnt/boot/esp  vfat  # ESP
/dev/mmcblk0p2  /mnt/boot      vfat  # Grub Boot
/dev/mmcblk0p3  /mnt           ext4  # Alpine Linux Root
</pre>

- [Asus T100 - 32-bit efi](https://github.com/lramage94/linux-asus-t100ta/tree/master/boot)
- [XDA - Asus T100TA Magic Stick](https://forum.xda-developers.com/windows-8-rt/win-8-development/live-asus-t100-ta-magic-stick-t3091481)

Alpine Linux Wiki Links
- [Bootable USB](https://wiki.alpinelinux.org/wiki/Create_a_Bootable_USB)
- [Chroot Install](https://wiki.alpinelinux.org/wiki/Installing_Alpine_Linux_in_a_chroot)
- [Dualboot HDD Install](https://wiki.alpinelinux.org/wiki/Installing_Alpine_on_HDD_dualbooting#Installing_Alpine_on_HDD)
- [Native HDD Install](https://wiki.alpinelinux.org/wiki/Native_Harddisk_Install_1.6)
- [UEFI Boot USB](https://wiki.alpinelinux.org/wiki/Create_UEFI_boot_USB)

I am having issues with installing grub.

<pre>
t100:~# apk add grub
ERROR: unsatisfiable constraints:
  grub (missing):
    required by: world[grub]
t100:~# 
</pre>

I also cannot find a instructions on how to compile a new kernel. The Wiki page for that is blank.

I do not own the charging cable that came with this tablet, and while a regular phone charger will work,
I cannot use the tablet and charge it concurrently, as the battery is drained faster than it is being charged.

Once I have this working, I will build an installer script to simplify the process.
