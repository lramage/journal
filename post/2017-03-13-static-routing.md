---
layout: post
title:  "Static routing with the linux kernel"
date:   2017-03-13 10:45:07
categories: log 
---

Here is a quick overview of my findings on static routing in the linux kernel from today.

First and formost, I found an odd quirk dealing with [ash](https://en.wikipedia.org/wiki/Almquist_shell "ash shell") in openwrt.
I was working with the [mwan3](https://wiki.openwrt.org/doc/howto/mwan3 "mwan3") package, which is used for load balancing and 
failover for wan connections in openwrt and I noticed some odd behaviour on my system. In /usr/sbin/mwan3track, we are monitoring
our wan interfaces at a specified interval, ( which is set via /etc/config/mwan3 ), and testing them to see if they are active.
For our specific use case, we were using mwan3 in failover mode and not for load balancing. So mwan3track would ping both
our wan (eth0) and wan2 (eth1) and if they responded properly then all would be well and they could go on their merry way.
However, If our wan interface failed a ping check, we would retry it a couple more times just to be sure, and then after our
defined number of retries, the mwan3track daemon would fire a hotplug event. On a hotplug event, mwan3 would delete the
static routes and iptables associated with our wan interface, and redirect our traffic to the wan2 interface. At least that is
how it is supposed to work in theory. What we found was that in part of the script, it was calling

<pre>
$ ps w 
</pre>

and for our image, the output was not matching. Openwrt, like most embedded operating systems, makes use of busybox for many of
the built in utilities that normally come with a GNU/Linux Distribution. So here is what was messing us up,

/bin/sh -> /bin/ash -> busybox

Here is our symlink.. I had no idea we could do that! And sometimes busybox will spit out commands it doesn't like.

Let's see, I also learned a few tricks about ip routes.

<pre>

$ ip route show table all # this command shows you all of our routing tables

$ cat << EOF >> /lib/mwan3/mwan3.sh

mwan3_delete_iface_default_route(){

  local default_ip

  default_ip=`ip route list dev $1 0/0 | awk '{ print $3 }'`

  ip route delete default via $default_ip
}

EOF

</pre>

This little function expects the interface name (eth0) to delete out the default route from the kernel routing tables.
Otherwise, after mwan3 runs, we are still not completely failed over until the kernel clears out the routing tables on it's own.

I also played with my new [hp envy 700](http://h20564.www2.hp.com/hpsc/doc/public/display?docId=c03782932) that I picked up this weekend.
It was used so I got it for cheap and it can run linux just fine. I am having problems with the [builtin wifi adapter](https://www.mediatek.com/products/broadbandWifi/rt3290 "rt3290") however. Also, I am trying to keep the [integrated graphics](http://www.amd.com/en-gb/products/graphics/desktop/oem/8670 "HD 8670D") enabled, and use a [secondary graphics card](http://www.amd.com/Documents/AMD_Radeon_HD_7470-HD_7450_GPU_Feature_Summary.pdf "HD 7450") for dual monitors.
