---
layout: post
title: just got a raspberry pi for pi day yesterday
date: '2016-03-15T19:19:58-04:00'
---
Just got a raspberry pi for pi day yesterday. 

My dilemma: No USB keyboard, No WiFi adapter, No display besides my TV.

Solution: RPi has HDMI cable plugged into TV, Ethernet cable plugged into laptop with Ubuntu which has a network bridge connecting the wired connection to the WiFi connection as well as an OpenSSH server on the laptop. Here is the good part, I am sitting on the couch with my netbook running Ubuntu, and I ssh into my laptop, from which I then ssh into the RPi.

Did you get all that?
