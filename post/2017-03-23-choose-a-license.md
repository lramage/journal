---
layout: post
title: Choose A License
date: 2017-03-23 4:37:21
categories: log philosophy coding
---

As I have been exploring the wonders of free and open source software, I have come
across different philosophies surrounding different software projects. There are so many different
licenses that all seem to promote the idea of "freedom" but which one is the best?

Well here is a nice little website to help you pick the right one for your project,

https://choosealicense.com/

Now there are some people out there that are totally against proprietary software. They believe 
that it is wrong and so they have come up with what are known as "Copyleft" licenses. You can 
read about what that is on the [GNU website](https://www.gnu.org/licenses/copyleft.en.html).
I can see some truths to this line of thinking. They want to make sure that big companies don't
just take an open source project and sell it without giving back any changes. And believe it or not,
that happens all the time. A quote comes to my
mind that I think of when building new software.

"Leave this world a little better than you found it." - Robert Baden-Powell

This is how we can make the world a better place, by doing what we can with what we have.
