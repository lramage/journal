---
layout: post
title: "Migrating from Subversion to Git"
date: 2017-04-19 07:19:58
categories: subversion git version control solutions migration jira
---

The company I work for is slowly but surely, transitioning away from subversion in favor of git for our version control system of choice. Thus, I spent my day reading and studying migrating repositories. There is much to take into consideration. In a corporate environment, there are large repositories that span over a decade or more. This makes me wonder, how much of this history do we bring with us? What is important and what can we throw away?

Humanity has been trying to preserve written information for a very long time. Well it is easier now, than ever before to keep track of information thanks to computers. We can store terabytes and petabytes of information. Do we even understand how big that is? And yet, there is so much digital refuse on the world wide web. 

So how do we decide what to keep and what to throw away? Version control is useful for when we mess up and need to restore to a pristine state, and also it can be useful for reflection so that we may learn from the mistakes that we have made. Even as I write this journal, I am sure there are flaws to be found. But that is one of the very reasons for my writings is so that one day I may be able to review my progress. But that is not the only reason. 

As I get older, I find myself forgetting about some of the lessons that I have learned. And particularly, when I was sick last year, I got out of the habit of practicing my programming and I had to relearn some of the concepts that I previously knew. This is also why I write. To help myself, and others to learn. Even if it is just to learn what not to do.

- [Create an Empty Branch in Git](http://www.bitflop.dk/tutorials/how-to-create-a-new-and-empty-branch-in-git.html)
