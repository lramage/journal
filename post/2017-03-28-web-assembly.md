---
layout: post
title: "Web Assembly"
date: 2017-03-28 3:03:26
categories: log web assembly
---

I am going to Atlanta for the next few days for work and during my time there
I would like to focus on learning about web assembly. Particularly, I would like
to build a static site generator using webgl that emulates a tiling window
manager. Here are a few references that I have found to help me in my endeavors.

References:
- [asm.js](http://asmjs.org/)
- [webassembly.org - high level goals](http://webassembly.org/docs/high-level-goals/)
- [MDN - Web Assembly](https://developer.mozilla.org/en-US/docs/WebAssembly)
