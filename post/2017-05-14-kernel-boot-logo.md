---
layout: post
title: "Kernel Boot Logo"
date: 2017-05-14 05:26:17
categories: linux kernel boot logo custom firmware
---

I did some research today on creating a custom boot logo for the linux kernel.
I need a logo. Currently, my logo just consists of my initials in a circle. 
I would like to create a new logo with geometric shapes, preferably a hexagon,
and I would like to have an intricate pattern or some clever typography.

- [Armadeus Wiki - Linux Boot Logo](http://www.armadeus.org/wiki/index.php?title=Linux_Boot_Logo)
- [Google Image Search - Firmware Icons](https://goo.gl/0Cs4bw)

I also am looking to replace jekyll with something more lightweight. I have looked at
[baker](https://github.com/taylorchu/baker), [bash blog](https://github.com/cfenollosa/bashblog),
[nanoblogger](http://nanoblogger.sourceforge.net/), [vee](https://github.com/estrabd/vee), [zodiak](https://github.com/nuex/zodiac), and a few others.
I would like a self contained, set of tools based on automake, awk, bash, and vim. 
I don't really need much more than that. I favor portability, and minimalism, but still able
to customize layouts and css as needed.

Here is my ideal workflow:

    $ git clone https://github.com/lramage94/lramage94.github.io.git
    $ make post
    # Opens markdown template with vim. (Date and Time are automatically inserted)
    # Edit post, including title and tags (optional), then save.
    # Saves file as <date-title>.md
    # Prompt will ask to either D)raft/E)dit/P)ost. (Just like bash blog does)
    $ git commit
    
I also will have a .config containing site variables (compared to _config.yml).
