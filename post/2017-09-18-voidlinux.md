---
layout: post
title: Void Linux
date: 2017-09-18 08:26:32
categories: void linux xbps
---

This weekend I finally got around to installing [Void Linux](https://www.voidlinux.eu/) on my Asus T100TA, 
and I must say, it's not too bad. It's been awhile since I've used a binary based distribution on one of
my own personal machines. I have also never used a distro with runit as the init system before. 
One thing that I bothers me about Void is the package manager. 
I do understand why all the commands are `xbps-<insert verb here>` when they could just be `xbps --<verb>` instead.
Not a huge deal, but I definitely prefer apk from Alpine, and of course portage from Gentoo. In fact,
I have been working on building a Gentoo-based firmware for this device in my [linux-asus-t100ta](github.org/lramage94/linux-asus-t100ta) repo.
It's not practical to compile anything on this device since it has some odd power issues. For one,
I do not have the original charger so I cannot just plug the device in and use it for long periods of time.
And also, the battery life is terrible. I basically use this device for tinkering with the touch screen, and 
occasionaly for gaming; although that has been less frequent due to losing the charger. 
I repaired the screen on this device a few years ago and was at one point, using this device for debugging wireless equipment
from a boom truck or ladder.
