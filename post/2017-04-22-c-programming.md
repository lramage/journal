---
layout: post
title: C Programming Style Guide
date: 2017-04-22 07:06:17
categories: c programming style guide nasa
---

It has been a few days since I have written. I have been rather busy learning and coding. I finally committed, no pun intended,
to my idea of forking dwm. I found a fork on google code that had some nice features that I liked and I am using that as my base.
There is no sense in reinventing features that are already there. It needs some work though. It is based on an older version of
dwm than I am used to. My feeble efforts can be found on github as [xwm](https://github.com/lramage94/xwm/tree/bleeding).

This code is not very readable and the dwm-plus fork is rather messy on top of that.
One of my goals will be to clean up this code and to improve on it so that I can use it for my custom linux distribution.
As I was researching C Styling Guides, I stumbled across the [NASA: C Style Guide](http://homepages.inf.ed.ac.uk/dts/pm/Papers/nasa-c-style.pdf).
