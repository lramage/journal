---
layout: post
title:  OpenWrt, mwan3, xrandr
date:   2017-03-14 11:00:47
categories: log 
---

Today I built a couple new openwrt images, with my own custom mwan3 fixes for failover. I added a function that will
delete the default route from our kernel routing tables and not just flush the wan routing table. I am not sure
what the difference between those things are but it seems to be working for now.

I also worked on my gentoo install for my new desktop computer (hp envy 700). I had to do a bit of reading up on
xrandr and X server configs.

<pre>
$ lspci | grep VGA # to find my graphics cards
00:01.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Richland [Radeon HD 8670D]
01:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Caicos PRO [Radeon HD 7450]

$ xrandr --listproviders
Providers: number : 2
Provider 0: id: 0x9a cap: 0xf, Source Output, Sink Output, Source Offload, Sink Offload crtcs: 4 outputs: 2 associated providers: 1 name:ARUBA @ pci:0000:00:01.0
Provider 1: id: 0x56 cap: 0xf, Source Output, Sink Output, Source Offload, Sink Offload crtcs: 4 outputs: 3 associated providers: 1 name:CAICOS @ pci:0000:01:00.0

$ xrandr --setprovideroutputsource 1 0 # connect our providers
$ xrandr --output VGA-1-1 --auto --right-of VGA-0 # set monitor to the right of the primary
</pre>

(And yes, I am using vga instead of hdmi.. If you feel sorry for me, feel free to buy me a longer hdmi cable.)

I put this in /etc/init.d/multi-monitor and added it to my [default runlevel](http://big-elephants.com/2013-01/writing-your-own-init-scripts/ "OpenRC")
