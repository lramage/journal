---
layout: post
title: "More iptables"
date: 2017-03-30 10:14:17
categories: linux networking iptables programming
---

So I spent my day working on virtual networking for Qemu and firewall rules
and I started reading about the linux kernel tcp stack. Further down the rabbit
hole, I found an interesting topic -
[Why Do We Use The Linux Kernel's TCP Stack?](http://jvns.ca/blog/2016/06/30/why-do-we-use-the-linux-kernels-tcp-stack/).

This leads into this project that I found on github:

[Network Stack in User Space](https://github.com/libos-nuse/net-next-nuse).

See also,
[Cloudflare: Why We Use The Linux Kernel's TCP Stack](https://blog.cloudflare.com/why-we-use-the-linux-kernels-tcp-stack/)
