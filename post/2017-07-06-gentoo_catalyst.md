---
layout: post
title: Gentoo Catalyst
date: 2017-07-06 09:37:06
categories: gentoo linux automated build system catalyst
---

It has been far too long since my last journal entry. I have recently begun learning more about 
[kconfig](https://www.kernel.org/doc/Documentation/kbuild/kconfig-language.txt), the language powering
the kernel build system aptly named kbuild.

I am looking to create a development environment similar to LEDE/Openwrt for building Hex OS, 
which is going to be my own linux distribution, heavily inspired and influenced by chromiumos 
but with an emphasis on containers. Ideally, I will have a relatively small base image with
a read only file system, and a persistent overlayfs for configuration changes. 
All external applications will be installed to task or workspace oriented containers. 

Because of this, I am learning more about [catalyst](https://wiki.gentoo.org/wiki/Catalyst),
which is a release building tool for Gentoo Linux. It does not have much documentation,
but I have enough to get started. I started a draft on the Gentoo Wiki that I will update with
any other information.
