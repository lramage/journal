---
layout: post
title: "Functional Programming on Android"
date: 2017-04-07 10:32:04
categories: android closure functional programming
---

I thought I'd share a presentation I discovered about Android development using
Closure and a REPL environment.

- [Doppioslash, Claudia. REPL your way into Android development](http://slides.com/doppioslash/repl-your-way-into-android-development-functionalkats)

Resources:
- [Cider](https://cider.readthedocs.io/en/latest/)
- [Clojure](https://clojure.org/)
- [Clojars](https://clojars.org/)
- [Leiningen](https://leiningen.org)
- [Lein-Droid](https://github.com/clojure-android/lein-droid)
- [Lein-Neko](https://github.com/clojure-android/neko)
- [Lein-Skummet](https://github.com/alexander-yakushev/lein-skummet)

Update:
- [The state of Clojure on Android](http://blog.ndk.io/state-of-coa.html)
